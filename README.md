How to open on Mac:

1. Go to "dist > osx > Investagrams-darwin-x64"
2. Open Investagrams app

![](Investagram-mac.gif)

How to open on Windows:
1. If you are on a 32-bit system, go to "dist > win > Investagrams-win32-ia32"
2. If you are on a 64-bit system, go to "dist > win > Investagrams-win32-x64"
3. Open Investagrams.exe

![](1.png)

![](2.png)

![](3.png)

![](4.png)