const electron = require('electron')
const app = electron.app
const BrowserWindow = electron.BrowserWindow

let window = null

app.on('ready', () => {
  const { width, height } = electron.screen.getPrimaryDisplay().workAreaSize
  win = new BrowserWindow({ width, height })
  win.loadURL('https://investagrams.com')
})
